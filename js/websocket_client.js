
var graphSocket;

var svg = d3.select("#graph");
var svgInfo = d3.select("#info svg");
var updateInProgress = document.getElementById("updateInProgress");
var width, height;

var color = d3.scaleOrdinal(d3.schemeCategory10);
var undefinedColor = "#aaa";

var simulation;

var currentPlayer = 0;

window.onload = newConnection();

function showAlertBox(waitingIcon=false) {
    if (waitingIcon) {
	document.getElementById("loadingIcon").style.display = "block";
    }
    document.getElementById("alertBox").style.display = "block";
}

function hideAlertBox() {
    document.getElementById("alertBox").style.display = "none";
    document.getElementById("loadingIcon").style.display = "none";
}

function updateNewGameMenu() {
    var graphTypeSelect = document.getElementById("graphType");
    var graphType = graphTypeSelect.options[graphTypeSelect.selectedIndex].value;
    document.getElementById("newGameButton").style.display = "block";
    document.getElementById("custom").style.display = "none";
    document.getElementById("pathcycle").style.display = "none";
    document.getElementById("proba").style.display = "none";
    switch(graphType) {
    case "path":
	document.getElementById("pathcycle").style.display = "block";
	break;
    case "cycle":
	document.getElementById("pathcycle").style.display = "block";
	break;
    case "random":
	document.getElementById("pathcycle").style.display = "block";
	document.getElementById("proba").style.display = "block";
	break;
    case "custom":
	document.getElementById("custom").style.display = "block";
	break;
    }
}

function newConnection() {
    showAlertBox(true);
    document.getElementById("newGame").style.display = "none";
    document.getElementById("gameBoard").style.display = "none";
    document.getElementById("info").style.display = "none";
    d3.select("#message").html("Connecting to game server...");
    graphSocket = new WebSocket(webSocketServerURL);

    graphSocket.onopen = function (event) {
	var msg = {
	    type: "hello",
	    date: Date.now()
	};
	graphSocket.send(JSON.stringify(msg));
    };
	
    graphSocket.onmessage = function (event) {
	var msg = JSON.parse(event.data);

	switch (msg.type) {
	case "hello":
	    hideAlertBox();
	    document.getElementById("newGame").style.display = "block";
	    updateNewGameMenu();
	    document.getElementById("gameBoard").style.display = "none";
	    d3.select("#message").html("");
	    currentPlayer = 0;
	    break;
	case "graph":
	    updateInProgress.style.display = "none";
	    hideAlertBox();
	    document.getElementById("newGame").style.display = "none";
	    document.getElementById("gameBoard").style.display = "block";
	    d3.select("#message").html("");
	    currentPlayer = msg.graph.current_player;
	    updateGraph(msg.graph);
	    updateInfo(msg.graph);
	    if (msg.incomplete) {
		document.getElementById("incomplete").style.display = "block";
	    } else {
		document.getElementById("incomplete").style.display = "none";
	    }
	}
    };
}

function newGame() {
    nb_players = parseInt(document.getElementById("nbplayers").value);
    
    svg.selectAll("g").remove();
    svg.append("g").attr("class", "links")
    svg.append("g").attr("class", "nodes")
    var windowWidth = window.innerWidth
	|| document.documentElement.clientWidth
	|| document.body.clientWidth;
    
    var windowHeight = window.innerHeight
	|| document.documentElement.clientHeight
	|| document.body.clientHeight;
    
    width = windowWidth - 350;
    height = windowHeight - 120;
    
    svg.attr("width", width);
    svg.attr("height", height);
    document.getElementById("info").style.display = "block";
    d3.select("#info h2")
	.attr("class", "")
	.html("Player information");
    svgInfo.attr("width", "100%");
    svgInfo.attr("height", (nb_players + 1) * 50);
    svgInfo.selectAll("g").remove();
    svgInfo.append("g")
	.attr("id", "players");
    for (var k = 0; k < nb_players; k++) {
	svgInfo.select("#players")
	    .append("g")
	    .attr("class", "player")
	    .attr("id", "player" + k)
	    .append("rect")
	    .attr("x", 0)
	    .attr("y", 0)
	    .attr("width", 260)
	    .attr("height", 50);	
	svgInfo.select("#player" + k)	
	    .attr("transform", "translate(0, " + 50 * k + ")")
	    .append("circle")
	    .attr("class", "node")
	    .attr("cx", 20)
	    .attr("cy", 25)
	    .attr("r", 10)
	    .attr("fill", color(k));
	svgInfo.select("#player" + k)
	    .append("text")
	    .text("Player " + (k + 1))
	    .attr("x", 40)
	    .attr("y", 32);
    }
    
    simulation = d3.forceSimulation()
	.force("link", d3.forceLink().id(function(d) { return d.id; }))
	.force("charge", d3.forceManyBody().strength(-500).distanceMax(500))
	.force("center", d3.forceCenter(width / 2, height / 2));

    // Construct a msg object containing the data the server needs to process the message from the chat client.
    var graphTypeSelect = document.getElementById("graphType");
    var graphType = graphTypeSelect.options[graphTypeSelect.selectedIndex].value;
    var msg = {
	type: "newgame",
	graphtype: graphType,
	nbplayers: nb_players,
	date: Date.now()
    };
    if (graphType == "path" || graphType == "cycle" || graphType == "random") {
	msg.nbvertices = parseInt(document.getElementById("nbvertices").value);
    }
    if (graphType == "custom") {
	msg.edges = JSON.parse(document.getElementById("customgraph").value);
    }
    if (graphType == "random") {
	msg.probability = parseFloat(document.getElementById("probability").value);
    }
    
    // Send the msg object as a JSON-formatted string.
    graphSocket.send(JSON.stringify(msg));
}
    
function updateInfo(graph) {
    svgInfo.selectAll(".player")
	.attr("class", function(d, i) {
	    if (!graph.can_play[i]) {
		return "player disabledPlayer";
	    }
	    return "player";
	})
	.select("text")
	.text(function(d, i) { return "Player " + (i + 1) + " (" + graph.scores[i] + " points)"; });
    svgInfo.select("#player" + graph.current_player)
	.attr("class", "player currentPlayer");
    if (graph.current_player === null) {
	graphSocket.close();
	svgInfo.selectAll(".player")
	    .attr("class", "player");
	d3.select("#info h2")
	    .attr("class", "alertMessage")
	    .html("End of Game! ");
    }
}


function updateGraph(graph) {
    var links = svg.select(".links")
	.selectAll("line")
	.data(graph.links);
    
    links.enter().append("line")
	.attr("stroke-width", function(d) { return Math.sqrt(d.value); });
    
    var nodes = svg.select(".nodes")
	.selectAll("circle")
	.data(graph.nodes);

    /* Enter selection of the nodes */
    nodes.enter().append("circle")
	.attr("class", "node")
	.attr("r", 10)
	.on("click", handleClick)
	.call(d3.drag()
	      .on("start", dragstarted)
	      .on("drag", dragged)
	      .on("end", dragended))
    /* Enter + Update selection of the nodes */
	.merge(nodes)
	.attr("fill", function(d) { return (d.owner !== undefined ? color(d.owner) : undefinedColor); })
	.attr("cursor", function(d) { return (d.players[currentPlayer] && d.owner === undefined ? "pointer": "auto"); })
	.on("mouseover", function(d) { if (d.players[currentPlayer] && d.owner === undefined) {d3.select(this).attr("fill", color(currentPlayer));} })
	.on("mouseout", function(d) { d3.select(this).attr("fill",  (d.owner !== undefined ? color(d.owner) : undefinedColor)); });

    /* The following loop fixes each node to its current position.
       It will prevent the simulation from restarting and pushing all nodes
       to their initial positions when a node is clicked. */
    
    simulation.nodes().forEach(function(d, i) {
	graph.nodes[i].fx = d.x;
	graph.nodes[i].fy = d.y;
    });

    /* Resets the simulation parameters to the new nodes and links */
    simulation
	.nodes(graph.nodes)
	.on("tick", ticked);
    
    simulation.force("link")
	.links(graph.links)
	.distance(10);
}

function ticked() {
    svg.selectAll(".links")
	.selectAll("line")
	.attr("x1", function(d) { return d.source.x; })
	.attr("y1", function(d) { return d.source.y; })
	.attr("x2", function(d) { return d.target.x; })
	.attr("y2", function(d) { return d.target.y; });
    
    svg.selectAll(".nodes")
	.selectAll("circle")
	.attr("cx", function(d) { return d.x; })
	.attr("cy", function(d) { return d.y; });
}


function handleClick(d, i) {
    if (d.players[currentPlayer] && d.owner === undefined) {
	var msg = {
	    type: "pick",
	    player: currentPlayer,
	    vertex: i,
	    date: Date.now()
	};

	updateInProgress.style.display = "block";
	graphSocket.send(JSON.stringify(msg));
    }
}


function dragstarted(d) {
    // Ugly workaround for a problem that arises from time to time when
    // clicking on a node.
    var cx = parseFloat(d3.event.sourceEvent.target.getAttribute("cx"));
    var cy = parseFloat(d3.event.sourceEvent.target.getAttribute("cy"));
    if (cx - d.x > 10 && cy - d.y > 10) {
	d.x = cx;
	d.y = cy;
    }
    simulation.nodes().forEach(function(d) {
	d.fx = undefined;
	d.fy = undefined;
    });
    if (!d3.event.active) simulation.alphaTarget(0.3).restart();
    d.fx = d.x;
    d.fy = d.y;
}

function dragged(d) {
    d.fx = d3.event.x;
    d.fy = d3.event.y;
}

function dragended(d) {
    if (!d3.event.active) simulation.alphaTarget(0);
    d.fx = null;
    d.fy = null;
}

