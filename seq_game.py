#!/usr/bin/python3

from functools import reduce
from itertools import combinations
from random import random
from timeout import timeout

PICKING_TIMEOUT = 5

class Graph:
    """Represents a sequential game"""

    def __init__(self, edges, nb_players, weights=None):
        self.players = [set() for _ in range(nb_players)]
        self.vertices = [set(range(nb_players)) for _ in range(len(edges))]
        self.edges = edges
        self.nb_vertices = len(self.vertices)
        self.nb_players = nb_players
        self.current_player = 0
        self.can_play = [True for _ in range(self.nb_players)]
        self.weights = weights if weights else [1] * self.nb_vertices

    def copy(self):
        new_graph = Graph([e.copy() for e in self.edges], self.nb_players)
        new_graph.players = [p.copy() for p in self.players]
        new_graph.vertices = [v.copy() for v in self.vertices]
        new_graph.current_player = self.current_player
        new_graph.can_play = self.can_play.copy()
        new_graph.weights = self.weights.copy()
        return new_graph

    def scores(self):
        return [sum(weight
                    for vertex, weight in enumerate(self.weights)
                    if vertex in self.players[player])
                for player in range(self.nb_players)]

    @timeout(PICKING_TIMEOUT)
    def pick(self, v, complete_verification=True):
        self._pick(self.current_player, v)
        # After a pick, we must update the list of allowed
        # players for each vertex
        if complete_verification:
            possible_players = self.list_solutions()
            self.vertices = possible_players

        # We must also update the list of vertices owned by
        # players
        for i, vertex in enumerate(self.vertices):
            assert vertex, "Inconsistent problem..."
            if len(vertex) == 1:
                self.players[next(iter(vertex))].add(i)

        # After that, we must update the set of players
        # that can still play...
        for player in range(self.nb_players):
            if self.can_play[player]:
                if not self._can_still_play(player):
                    self.can_play[player] = False

        # Then, we move to the next player that can still play
        for _ in range(self.nb_players):
            self.current_player = (self.current_player + 1) % self.nb_players
            if self.can_play[self.current_player]:
                return

        # If no player is found, the current_player is set
        # to None, meaning that the game is finished...
        self.current_player = None

    def _can_still_play(self, player):
        return reduce(lambda x, y: x or (player in y[1]
                                         and y[0] not in self.players[player]),
                      enumerate(self.vertices),
                      False)

    def _pick(self, player, v):
        assert player in self.vertices[v],\
            "Player {} cannot pick vertex {}!".format(player, v)
        self.vertices[v] = {player}
        self.players[player].add(v)
        # Graph update currently deactivated
        while self._update_graph():
            pass

    def _update_graph(self):
        changed = False  # Did anything change during this update?
        for source, lp in enumerate(self.vertices):
            # For each vertex...
            new_lp = []
            for player in lp:
                # ...and for each player that can still take this vertex
                # we will check whether this vertex can still reach
                # a vertex that belongs to the player
                reachable = True
                if self.players[player]:
                    # if the player already owns vertices
                    reachable = False
                    for target in self.players[player]:
                        if self.is_connected(source, target, player):
                            reachable = True
                            break
                if reachable:
                    # If the vertex is still reachable, or the player
                    # does not own any vertex so far...
                    new_lp.append(player)
                else:
                    changed = True
            if changed:
                self.vertices[source] = new_lp
                # If after having removed the player the vertex only
                # have one player left, then we should add this vertex
                # to the set of vertices owned by this player
                if len(new_lp) == 1:
                    self.players[next(iter(new_lp))].add(source)
        return changed

    def list_solutions(self, possible_players=None, n=None):
        """
        This function runs a backtracking algorithm to list the
        set of players that are still possible for each vertex.
        """
        if n is None: # Root
            n = self.nb_vertices - 1
            possible_players = [set() for _ in range(self.nb_vertices)]
        if n == -1: # Leaf
            connected = True
            # We check whether this is indeed a solution
            for player, list_v in enumerate(self.players):
                # For each player p
                for i, j in combinations(list_v, 2):
                    # For each pair of p's vertices
                    if not self.is_connected(i, j, player):
                        # We check whether it is connected or not
                        connected = False
                        break
            if connected: # It is indeed a solution
                for player, list_v in enumerate(self.players):
                    for vertex in list_v:
                        possible_players[vertex].add(player)
            return possible_players
        for player in self.vertices[n]:
            vertices = [v.copy() for v in self.vertices]
            players = [p.copy() for p in self.players]
            self._pick(player, n)
            self.list_solutions(possible_players, n - 1)
            self.vertices = vertices
            self.players = players
        return possible_players

    def is_connected(self, i, j, player, marked=None):
        if not marked:
            marked = set()
        if i == j:
            return True
        marked.add(i)
        for k in self.edges[i]:
            if k not in marked and player in self.vertices[k]:
                if self.is_connected(k, j, player, marked):
                    return True
        return False

    def as_node_link(self):
        serialized_object = {}
        serialized_object["nodes"] = [
            {"id": "v{}".format(i),
             "players": [1 if p in self.vertices[i] else 0
                         for p in range(self.nb_players)]}
            for i in range(self.nb_vertices)
            ]
        for player, vertices in enumerate(self.players):
            for k in vertices:
                serialized_object["nodes"][k]["owner"] = player
        serialized_object["links"] = [
            {"source": "v{}".format(source),
             "target": "v{}".format(target)}
            for source, neighbours in enumerate(self.edges)
            for target in neighbours
        ]
        serialized_object["current_player"] = self.current_player
        serialized_object["can_play"] = self.can_play
        serialized_object["scores"] = self.scores()
        return serialized_object

    @staticmethod
    def generate_path(nb_vertices, nb_players):
        return Graph([{1}]
                     + [{k - 1, k + 1} for k in range(1, nb_vertices - 1)]
                     + [{nb_vertices - 2}], nb_players)

    @staticmethod
    def generate_cycle(nb_vertices, nb_players):
        return Graph([{1, nb_vertices - 1}]
                     + [{k - 1, k + 1} for k in range(1, nb_vertices - 1)]
                     + [{0, nb_vertices - 2}], nb_players)

    @staticmethod
    def generate_random(nb_vertices, nb_players, probability=0.5):
        """
        Generates a random graph using the Gilbert generation model
        (stating that each edge has an independant probability p to
        be present).
        """
        edges = [set() for _ in range(nb_vertices)]
        for i, j in combinations(range(nb_vertices), 2):
            if random() <= probability:
                edges[i].add(j)
                edges[j].add(i)
        return Graph(edges, nb_players)
