#!/usr/bin/python3

import unittest
from itertools import combinations
from nose_parameterized import parameterized
from seq_game import Graph

GRAPH1 = Graph([{1, 3}, {0, 2}, {1, 5},
                {0, 4, 6}, {3, 5, 7}, {2, 4, 8},
                {3, 7}, {4, 6, 8}, {5, 7}],
               2)


class TestInstances(unittest.TestCase):
    def setUp(self):
        self.path = Graph.generate_path(10, 2)
        self.cycle = Graph.generate_cycle(12, 2)
        self.graph1 = GRAPH1

    @parameterized.expand([(Graph.generate_path(10, 2), ),
                           (Graph.generate_cycle(12, 2), ),
                           (GRAPH1, )])
    def test_connected(self, graph):
        for vertex1, vertex2 in combinations(range(len(graph.vertices)), 2):
            self.assertTrue(graph.is_connected(vertex1, vertex2, 0))
            self.assertTrue(graph.is_connected(vertex1, vertex2, 1))

    def test_connected_after_picked(self):
        self.path._pick(0, 3)
        actual = self.path.is_connected(2, 6, 1)
        self.assertFalse(actual)
        self.cycle._pick(0, 3)
        actual = self.cycle.is_connected(2, 6, 1)
        self.assertTrue(actual)
        self.graph1._pick(0, 3)
        self.graph1._pick(0, 4)
        actual = self.graph1.is_connected(0, 7, 1)
        self.assertTrue(actual)
        self.graph1._pick(0, 5)
        actual = self.graph1.is_connected(0, 7, 1)
        self.assertFalse(actual)

    def tearDown(self):
        pass


if __name__ == "__main__":
    import nose
    import sys

    MODULE_NAME = sys.modules[__name__].__file__

    nose.run(argv=[sys.argv[0], MODULE_NAME, '-v'] + sys.argv[1:])
