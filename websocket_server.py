#!/usr/bin/python3

# WS server example

import asyncio
import websockets
import json
from seq_game import Graph
import websockets.exceptions
import logging
import sys
from timeout import TimeoutError

import server_config

current_graphs = {}

@asyncio.coroutine
def send_hello(websocket):
    logging.debug("Sending hello to user {}".format(websocket))
    yield from websocket.send(json.dumps({"type": "hello"}))

@asyncio.coroutine
def send_graph(websocket, incomplete=False):
    logging.debug("Sending graph: " + str(current_graphs[websocket].as_node_link()))
    yield from websocket.send(json.dumps({
        "type": "graph",
        "graph": current_graphs[websocket].as_node_link(),
        "incomplete": incomplete}))
    
@asyncio.coroutine
def game(websocket, path):
    global current_graph
    try:
        while True:
            message = yield from websocket.recv()
            msg = json.loads(message)
            logging.info(msg)
            if msg['type'] == "hello":
                current_graphs[websocket] = None
                yield from send_hello(websocket)
            elif msg['type'] == "newgame":
                if msg['graphtype'] == 'path':
                    current_graphs[websocket] = Graph.generate_path(msg['nbvertices'], msg['nbplayers'])
                elif msg['graphtype'] == 'cycle':
                    current_graphs[websocket] = Graph.generate_cycle(msg['nbvertices'], msg['nbplayers'])
                elif msg['graphtype'] == 'custom':
                    current_graphs[websocket] = Graph(msg['edges'], msg['nbplayers'])
                elif msg['graphtype'] == 'random':
                    current_graphs[websocket] = Graph.generate_random(msg['nbvertices'], msg['nbplayers'], msg['probability'])
                yield from send_graph(websocket)
            elif msg['type'] == "pick":
                vertex = msg["vertex"]
                current_graph = current_graphs[websocket].copy()
                try:
                    current_graphs[websocket].pick(vertex)
                    yield from send_graph(websocket)
                except TimeoutError:
                    logging.info("Timeout error on picking operation")
                    current_graphs[websocket] = current_graph.copy()
                    current_graphs[websocket].pick(vertex, False)
                    yield from send_graph(websocket, True)
            else:
                logging.error("Unknown message type: {}".format(msg))
    except websockets.exceptions.ConnectionClosed:
        current_graphs.pop(websocket)
        logging.info("Websocket closed...")

logging.basicConfig(level=server_config.LOG_LEVEL,
                    filename=server_config.LOG_FILE)

start_server = websockets.serve(game, server_config.URL,
                                server_config.PORT)

asyncio.get_event_loop().run_until_complete(start_server)
asyncio.get_event_loop().run_forever()
